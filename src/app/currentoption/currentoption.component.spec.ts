import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentoptionComponent } from './currentoption.component';

describe('CurrentoptionComponent', () => {
  let component: CurrentoptionComponent;
  let fixture: ComponentFixture<CurrentoptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentoptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentoptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
